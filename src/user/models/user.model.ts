import { Document } from 'mongoose';

export interface User extends Document {
  id: string;
  fullName: string;
  phone: string;
  email: string;
}

export interface UserReq extends Document {
  fullName: string;
  phone: string;
  email: string;
}
