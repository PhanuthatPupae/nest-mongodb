import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseInterceptors,
} from '@nestjs/common';
import { ResponseInterceptor } from '../shared/interceptor/response.interceptor';
import { User, UserReq } from './models/user.model';
import { UserService } from './user.service';
import { ResponseController } from '../shared/models/response.model';

@UseInterceptors(ResponseInterceptor)
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('/list')
  async Getusers(): Promise<ResponseController<User[]>> {
    const users = await this.userService.getUsers();
    return { result: { message: 'success', data: users } };
  }

  @Get('/:id')
  async GetuserByID(
    @Param('id') id: string,
  ): Promise<ResponseController<User>> {
    try {
      const user = await this.userService.getUserByID(id);
      return { result: { message: 'success', data: user } };
    } catch (error) {
      return { result: { message: 'not found', data: error } };
    }
  }

  @Post()
  async createUser(@Body() model: UserReq): Promise<ResponseController<User>> {
    try {
      await this.userService.createUser(model);
      return { result: { message: 'create success' } };
    } catch (error) {
      return { result: { message: 'create fail' } };
    }
  }
}
