import * as mongoose from 'mongoose';

const UserSchema = new mongoose.Schema({
  fullName: { type: String, default: '' },
  email: { type: String, default: '' },
  phone: { type: String, default: '' },
});

export default UserSchema;
